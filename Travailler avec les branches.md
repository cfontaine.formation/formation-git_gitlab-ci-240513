# Travailler avec les branches
## Étiquettes
On copie le dossier 01-base et on le renomme en 08-branche
```sh
git log

# Créer un tag léger sur le commit où l'on se trouve
git tag 3.0.0
git log

# Créer un tag léger sur le commit 103979acd19c7
git tag 1.0.0 103979acd19c7
git log

# Créer un tag annoté sur le commit a4852fa2840e4a
git tag -a 1.1.0 a4852fa2840e4a
# Ouvre l’éditeur de texte pour la saisie du message du tag
git log

# Lister les étiquettes
git tag --list

# Lister les étiquettes en fixant le nombre de ligne du message -> 2
# Si c'est un tag annoté -> c'est le message du tag qui est affiché
# Si c'est un tag léger -> c'est le message du commit qui est affiché
git tag --list -n2

git log
git tag -a 1.2.0 b451cb2ed6 -m "on a ajouter des fonctionnalités"

# Lister selon un motif
git tag --list '1.*' # 1.0.0 1.1.0 1.2.0

# Visualiser les détails d’un tag léger -> affiche les détails du commit
git show 3.0.0

# Visualiser les détails d’un tag annoté -> affiche les détails du commit et du tag
git show 1.1.0

# Supprimer un tag (le commit associer n'est pas effacé)
git tag -d v1.2.0
```
## Branches
```sh
git init -b main

touch user.java
git add . 
# Quand on crée un commit, la branche main avance sur se commit
git commit -m "ajout de user"

touch article.java
git add .
# À chaque commit, la branche main se déplace pour rester en permanence sur le dernier commit
git commit -m "ajout d'article"

# Création de la branche dev
git branch dev
git graph # alias qui correspond à git log --graph --oneline --all

# Lister les branches
git branch 
# * main  * -> marque la branche courante
#   dev

# -v, permet de lister les branches avec en plus l’empreinte et le message du dernier commit
git branch -V

# Quand on ajoute, un commit il se trouve sur le branche courante -> main
git add .
git commit -m "ajout du code dans la classe user"
git graph

# checkout -> pour se positionner sur la branche dev
git checkout dev
# Après un positionnement
# - Le répertoire de travail est modifié
# - L’index n’est pas modifié
# - HEAD est placé sur le commit le plus récent de la branche
git branch  # dev devient la branche courante
# Les commits sont maintenant ajouter sur la branche dev
git add .
git commit -m "modification de la classe Article"
git graph

# Revenir sur la branche précédente  
git checkout -
# Création de la branche fixbug et positionnement sur cette branche
git switch -c "fixbug"
# Revenir sur la branche précédente, (idem git checkout -)
git switch -

# Fusionner la branche dev dans la branche main
# Comme il y a une divergence c'est un three way merge -> creation d'un commit de fusion
git merge dev
git graph

# On se positionne sur la branche fixbug
git switch fixbug 
# On fusionne la branche main avec la branche fixbug
# Il n'y a pas de divergence c'est un fast-foward merge -> la branche fixbug va être déplacé et va rattrapé la branch main 
git merge main
git graph
# idem pour la branche dev, elle va rattraper la branche main
git switch dev
git merge main
git graph

git add .
git commit -m "ajout du prix"
git add .
git commit -m "ajout de la description"
git graph

# On ajoute un commit  sur chaque branche main et dev
# qui modifie dans le même fichier la même méthode  
git switch main
git merge dev
git graph
git add .
git commit -m "ajout de la méthode afficher"
git graph
git switch dev
git add .
git commit -m "ajout de la méthode afficher dans Article"
git graph
git switch main

# On fusionne la branche dev dans la branche main
# comme on fournit sur chaque branche pour une méthode 2 codes différents
# git ne peut pas choisir l'un ou l'autre et ne peut pas faire la fusion automatiquement
# -> on a un conflit
git merge dev
# git status va lister tous les fichiers en conflit de fusion
git status

# l'option --abort permet d'annuler la fusion et de revenir dans l'état précédent 
git merge --abort
git graph

# on relance la fusion
git merge dev
# git status indique que qu'il y a un conflit dans le fichier article
git status

# On édite le fichier article.java
# <<<<<<< HEAD: article.java.html
# contenu de la branche main
# =======
# contenu de la branche dev
# >>>>>>> dev : article.
# On résout le conflit en choisissant une des 2 versions ou en écrivant un autre code

# Pour indiquer que le conflit est résolue, on ajoute le fichier à l'index 
git add article.java
git status
# Quand tous les conflits sont résolue, on crée le commit de fusion
git commit
git graph

# On crée un nouveau conflit dans le fichier article.java 
git add article.java 
git commit -m "ajout commentaire"
git switch dev
git add article.java 
git commit -m "ajout commentaire 2"
git switch -
git merge dev
git status
# On peut utiliser checkout avec --ours (version de main) ou avec --theirs (version de dev) pour résoudre le conflit sans éditer le fichier
git checkout --theirs article.java
git status
# On indique que le conflit est résolue
git add .
git status
# On crée le commit de fusion
git commit
# Suppression de la branche fixbug 
git branch -d fixbug
git graph

# on fait un fast foward fusion  -> dev rattrape main
git switch dev
git merge main

# on crée des commits sur la branche de dev 
git add .
git commit -m "1"
git switch main
git add .
git commit -m "2"
git graph

# Lister les branches qui ont fusionnées
git branch --merge
# Lister les branches qui n'ont pas fusionnées
git branch --no-merge
# On ne peut pas supprimer dev car elle n'a pas fusionnée avec l'option -d
git branch -d dev
# Mais on peut forcer la suppression avec -D
git branch -D dev
git graph
git branch no-merge 

# git checkout -b ou git switch -c -> on crée un nouvelle branche et on se positionne direct sur celle-ci 
git checkout -b other
```