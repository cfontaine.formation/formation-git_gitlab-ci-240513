#  Annuler des actions
On copie le dossier 01-base et on le renomme en 06-annulation
```sh
touch Change.md
git add .
git commit -m "Ajout de Change.md"

# On modifie le fichier Change.md
echo "Liste des modifications du projet" >> Change.md
git add Change.md
git status -s

# On peut modifier le dernier commit pour ajouter la modification de Change.md
git commit --amend # -> l’éditeur de texte est ouvert pour sair le message et le commit est ré-écrit
# Pour modifier le commit avec --amend, il ne doit pas être envoyé sur un dépôt distant

# On peut modifier le dernier commit en saisissant le message avec l'option -m
git commit --amend -m "Ajout des autres fichiers du projet"

# On peut modifier le dernier commit sans modifier le message  avec l'option --no-edit
echo "------------" >> Change.md
git add . 
git commit --amend --no-edit

# Si le commit a été envoyé sur un dépôt distant, on peut ajouter un commit qui annule le précédent
# On peut annuler le commit 3f0301269a (avec le message réorganisation du projet)
git show 3f0301269a
git revert 3f0301269a
# Un commit a été ajouté, qui est l'inverse du commit 3f0301269a
git log -10

# avec l'option -n, l'inverse du commit 13618b8c90e5e est ajouté uniquement dans l'index
git revert -n 13618b8c90e5e
git log -10
git diff --staged

# git reset
git reset --soft HEAD~4  # on déplace HEAD et la branche main de 4 commits en arrière
git graph # graph -> alias de log --graph --oneline --all
# Les modifications des 4 commits sont placées dans l'index
# git reset --soft peut être utilisé pour fusionner plusieurs commits
git status -s
git commit -m "fusion des 4 commits"
git graph
git status -s

touch Marque.java à 
git add Marque.java
git status -s # -> A Marque.java
git reset # Suppression des modifications de l’index (correspond à git reset HEAD)
git status -s # -> ?? Marque.java -> untracked

nano Article.java # on modifie le fichier Article.java
git status -s # M  Article.java 
              # ?? Marque.java
git reset --hard # Suppression des modifications du répertoire de travail (correspond à git reset --hard HEAD)
# Tous les fichiers suivies en version vont revenir tel qu'ils sont dans le dernier commit
# /!\ git reset --hard n'est pas réversible, les données seront définitivement perdues
git status -s # ?? Marque.java # les fichiers untracked ne seront pas supprimés

# git checkout permet de revenir à un commit, uniquement HEAD est déplacé, pas la branche main
# le répertoire de travail est modifié pour correspondre au commit
git checkout HEAD~5 # On déplace HEAD de 5 commit en arrière
git log --oneline
git graph
git checkout main # On replace HEAD sur la branche main 
git graph

# detached HEAD 
git checkout HEAD~5 # On déplace HEAD de 5 commit en arrière
git graph
touch Fournisseur.java 
git add .
# On crée un commit avec un detached head
git commit -m "Ajout Fournisseur.java"
git graph
git checkout main # lorsque l'on replace HEAD sur la branche main, le commit créé n'est plus accessible
git graph


touch test1.sql test2.sql db.properties
git add .
git status -s

#  Revenir à un état antérieur du fichier dans l’index
git reset -- test*.sql  # les 2 fichiers test1.sql test2.sql sont retirés de l'index 
git status -s

# Revenir à un état antérieur d'un fichier
git checkout e04ae17114df README.md # le fichier README.md revient tel qu'il était dans le commit e04ae17114df (vide)
nano README.md 

# On annule les modifications de README.md
git reset --hard

git add test1.sql # On ajoute test1.sql à l'index
git status -s

git restore --staged test1.sql # on désindexe le fichier test1.sql 
git status -s
echo "dfbkzbckjrcbk" >> README.MD

git restore README.md # on réinitialise le fichier avec celui présent dans le dernier commit 
nano README.md 

git log --oneline
git restore --source e04ae17 README.md # on restaure le fichier avec celui du commit e04ae17
nano README.md 

git reflog # affiche toutes les références prises par HEAD
```