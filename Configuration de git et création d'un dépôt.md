# Configuration de git et création d'un dépôt
```sh
# Afficher la version de git pour tester l'installation de git
git --version

# Afficher l'aide des commandes porcelaine (commande utilisée couramment)
git help

# Afficher l'aide des commandes porcelaine + plomberie (commande bas niveau)
git help -a

# Afficher la documentation de la commande commit
git help commit
# ou
git commit --help

# Configuration minimale requise nom et email
# --global -> configuration pour l’utilisateur du système
git config --global user.name "John Doe"
git config --global user.email jdoe@dawan.fr 

# Configurer l’éditeur de texte utilisé par git
git config --global core.editor nano

# Supprimer la pagination
git help -a # l'aide s'affiche page par page 
git config --global core.pager ''
git help -a # l'aide s'affiche sans pagination 

# Afficher toutes les configurations
git config --list

# Afficher la valeur du paramètre user.email -> jdoe@dawan.fr
git config user.email

# Connaitre le fichier, où le paramètre core.autocrlf a été définie et sa valeur -> file:C:/Program Files/Git/etc/gitconfig true (--system)
git config --show-origin core.autocrlf

# Configuration au niveau utilisateur (--global) de core.autocrlf
git config --global core.autocrlf false

# Connaitre le fichier, où le paramètre core.autocrlf a été définie et sa valeur -> file:C:/Users/Jehann/.gitconfig false (--global)
git config --show-origin core.autocrlf

# On supprime la configuration de core.autocrlf au niveau utilisateur 
git config --global --unset core.autocrlf

# Il ne reste que la configuration --system  pour core.autocrlf -> true
git config --list

# Création d’un dépôt local dans le dossier 01-base et avec une branche par défaut qui a pour nom main
git init -b main 01-base

# Cloner le dépôt du projet normalize.css dans le dossier 02-visualisation
git clone https://github.com/necolas/normalize.css.git 02-visualisation

# Cloner les 5 derniers commit du dépôt du projet normalize.css
git clone https://github.com/necolas/normalize.css.git --depth 5

cd normalize.css/
git log # Afficher la liste des commits -> les 5 commits les + récent

# Cloner les 5 commits suivant du dépôt de normalize.css
git fetch --depth 10
git log # Afficher la liste des commits -> les 10 commits les + récent

# Cloner tous les commits  du dépôt de normalize.css
git fetch --unshallow
git log # Afficher la liste des commits -> les 320 commits du dépôt

# Suppression du dossier normalize.css
cd ..
rm -rf  normalize.css
```