# Ignorer des fichiers
On décompresse le fichier __gitignore.zip__ dans le dossier __01-base__

```sh
git status -s

# À la racine du projet, le fichier .gitignore, contient la liste des règles des fichiers à ignorer

nano .gitignore
# on ajoute les règles au fichier .gitignore, pour ignorer:
# - le fichier .project à la racine du projet
# - le dossier bin et son contenu
# - tous le fichiers xml sauf le fichier pom.xml
# - tous le fichiers log
# - tous les chemins qui commencent par log et qui finissent par daily

```

### .gitignore
```sh
# Les fichiers à ignorer
# Règle pour ignorer le fichier .project qui ce trouve à la racine du projet
/.project

# Règle pour ignorer tous les fichiers xml
*.xml

# Règle pour ignorer le dossier bin et tous se qu'il contient
bin/

# Règle pour ignorer tous les fichiers log
*.log

# Tous les fichiers xml sont ignorés sauf le fichier pom.xml
!pom.xml

# Ignore tous les chemins qui commencent par log et qui finisent par le dossier daily
log/**/daily/
```

```sh
# Avec l’option -u, git status affiche le contenu des dossiers
git status -u

# Pour déboguer le fichier.gitignore: 
# affiche la règle qui permet d'ignorer le fichier passé en paramètre 
git check-ignore -v setting.xml  # affiche -> .gitignore:7:*.xml      config.xml
git check-ignore -v log/daily/ 

# Le fichier .gitignore doit être versionné
git add .
git commit -m "ajout .gitignore"

# On peut ajouter des fichiers .gitignore dans les sous-répertoires, 
touch src/.gitignore
# On ajoute dans ce fichier la règle *.yml
echo *.yml >>  src/.gitignore
# Tous les fichiers yaml sont ignorés dans le dossier src et ses sous-dossiers
# Le fichier .gitlab-ci.yml à la racine du projet n'est pas ignoré
git status -u
git add .
git commit -m "Autre fichier .gitignore"

# On peut définir un fichier pour ignorer les fichiers propres à l’utilisateur (fichier de configuration d’IDE …)
# exemple on ajoute au fichier .gitignore_global, la règle pour ignorer le fichier .idea, généré par l'IDE intelij
# /.idea
git config --global core.excludesfile ~/.gitignore_global
git status -u

```