# Travailler avec un dépôt distant

```sh
# On va créer 3 dépôts: 1 serveur + 2 dépôts de développeur qui souhaite partager leur travail
# On utiliser comme protocole réseau: le système de fichier 


# Dans un premier terminal, on crée le serveur en  utilisant l'option --bare qui permet de créer un dépôt brut -> sans répertoire de travail
git init --bare -b main serveur
cd serveur 

# Dans un deuxième terminal, on crée le dépôt du développeur A
git init -b main devA
cd devA

# On liste les dépôt distant -> il n'y a pas de dépôt distant 
git remote

# On va ajouter un dépôt distant
git remote add serv "C:\Dawan\Formations\git\09-depot distant\serveur"

# On a un serveur distant qui s'appelle serv
git remote

# idem mais donne en plus l’adresse du dépôt (fetch et push)
git remote -v 

# On crée un commit 
touch User.java
git add User.java 
git commit -m "ajout de User"
git log

# On pousse le commit vers le dépôt distant
git push serv main

# Dans le terminal du serveur
git log
# On voit que le commit est présent dans le dépôt du serveur 

# Dans un troisième terminal, le développeur B va cloner le dépôt
git clone  "/c/Dawan/Formations/git/09-depot distant/serveur" devB
cd devB
git log # le commit est présent dans le dépôt du développeur B

# git clone a déjà configuré le dépôt distant, par défaut le dépôt distant est nommé origin
git remote -v

# Le développeur B crée un nouveau commit
touch Commande.java
git add Commande.java 
git commit -m "ajout de Commande"
git log

# Il pousse le commit vers le dépôt distant
git push origin main

# Dans le terminal du serveur
git log
# On voit que le 2ème commit est présent dans le dépôt du serveur 

# Dans le terminal du développeur A, on veut récupérer le commit du développeur B
# Avec git fetch, on récupère les données de dépot distant vers le dépôt local
# Dans sa propre branche, il n'y a pas de modification du répertoire de travail 
git fetch serv

# l'option -a de git branch permet d'afficher les branches cachées
git branch -a
# Il y a une branche en plus qui apparait remotes/serv/main c'est une branche de suivie distante.
# Elle correspond à la position de la branche main sur le serveur lors de la dernière connexion vers le serveur

git graph
# On obtient
# * f2f8e50 (serv/main) ajout Commande .java
# * 38b196f (HEAD -> main) ajout de User

# On va fusionner dans la branche main la branche de suivie distante serv/main
git merge serv/main
git graph
# La branche main va avancer d'un commit sur lequel se trouve la branche de suivie distant

# Le développeur A va créer un commit et le pousser vers le dépôt distant
touch Article.java
git add Article.java 
git commit -m "ajout de Article"
git push origin main

# Dans le terminal du développeur B, on va récupérer ce commit avec git pull
# git pull -> tire (git fetch) et fusionne la branche distante dans la branche locale
git pull origin

# Le développeur B va créer une branche dev et y créer un commit  
git switch -c dev
touch Produit.java
git add Produit.java 
git commit -m "ajout de Produit"

# Comme la branche dev est inconnue du dépôt distant, il faut ajouter l'option -u à git push
git push -u origin main

# Sur le terminal du serveur
git graph # une branche dev est créé

# le développeur B veut récupérer ce commit
git fetch # une branche de suivie serv/dev est créée

## on va créer la branche locale qui correspond avec l'option --track de checkout
git checkout --track serv/dev

# On peut inspecter un dépôt distant
git remote show serv
```