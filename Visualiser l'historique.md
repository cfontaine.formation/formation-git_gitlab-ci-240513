Visualiser l'historique
```sh
cd 02-visualisation/
# Lister les commits
# le 1er commit affiché est le plus récent, le dernier commit est le plus ancien (le premier commit réalisé)  
git log

# Afficher les 5 commits les plus récent
git log -5

# Lister les commits sur une ligne , il n'y a que la première ligne du message du commit qui est affiché  
git log --oneline

# Afficher les 20 commits les plus récent sur une seule ligne
git log --oneline -20

# Affichage semi graphique de l'historique des commits
git log --graph --oneline --all

# --pretty -> permet de personnaliser l'affichage des informations de git log
# exemple de format: 	%h -> signature SHA1 abbrégé du commit, %cn -> nom du validateur
# 						%n -> passer à la ligne suivante, %s -> titre du message du commit
git log --pretty=format:"%h %cn %n %s"

# On peut modifier la couleur de l'affichage du texte
# %Cred -> rouge, %Cgreen -> vert, %Cblue -> bleu
# %Creset -> pour revenir à la couleur initiale
git log --pretty=format:"%Cred %h %cn %Creset %n %s"

# Il existe des formats prédéfinis short (Le - d'information), medium,full ,fuller (Le + d'information)
git log --pretty=short
git log --pretty=fuller

# Créer un alias (logperso) pour la commande git log avec un format personnalisé 
git config --global alias.logperso 'log --pretty=format:"%Cred %h %cn %Creset %n %s"'
# Utilisation de l'alias 
git logperso

# l'alias fait partie de la configuration de git
git config --list

# Création d'un alias gr qui permet d'afficher l'historique des commits de manière semi-graphique
git config --global alias.gr 'log --graph --oneline --all'
git gr

# ! -> permet de créer un alias (visu) vers une commande externe à git (gitk) 
git config --global alias.visu '!gitk'
git visu
git config --list

# Filtrer par date
# afficher tous les commits réalisés avant le 1er janvier 2015
git log --before="2015-01-01"

# afficher tous les commits réalisés après le 1er septembre 2014
git log --after="2014-09-01"

# afficher tous les commits réalisés entre le 1er septembre 2014 et le 1er janvier 2015 
git log --before="2015-01-01" --after="2014-09-01"

# Afficher les commit réalisés depuis 5 ans
git log --since = "5 year ago"

# Afficher les commit réalisé depuis 6 ans et jusqu’à 5 ans et 4 mois
git log --since = "6 year ago" --until= "5 year 4 month ago"

# Filtrer par auteur
# Lister les commits dont Andrey Chechkin est l'auteur du code
git log --author "Andrey Chechkin"

# --author : celui qui a réalisé le code
# --committer : celui qui va recevoir, valider et intégrer le code au dépôt (validateur)

touch TODO.txt
git add .
# l'option --author permet d'indiquer que l'auteur du code est John Doe, Le commiter est celui qui exécute la commande
# par défaut celui qui exécute la commande git commit est l'auteur et le commiter
git commit --author="John Doe<jd@dawan.com>" -m "toDO"

git log --pretty=fuller -1

# Lister les commits que j'ai réalisé
git log --committer "Christophe Fontaine"

# Lister les commits dont Ismael Martínez ou Andrey Chechkin sont l'auteur du code
git log --author "Ismael Martínez\|Andrey Chechkin"

# Regrouper les commits par auteurs et afficher la première ligne de chaque message
git shortlog
# L’option -n permet de trier le auteurs par nombre de commit qu'ils ont réalisé
git shortlog -n

# Filtrer par fichier et rechercher
# Lister tous les commits qui concernent le fichier README.md
git log --follow README.md
git log -- README.md TODO.txt

# --grep -> Lister les commits dont le message contient des mots clés

# Lister les commits dont le message contient le mots update
git log --grep=update

# Retourner les commits qui introduisent qui ajoutent ou retirent le texte line-height
git log -S "line-height"

# Afficher les commits de fusion
git log --merges

# Afficher tous les commits sauf les commits de fusion
git log --no-merges

# Afficher tous les 6 commits en arrière entre l'avant dernier commit réalisé (HEAD) 
git log HEAD~6.. HEAD~

# Afficher les statistiques: Affiche le nombre de ligne ajouté (+), ligne supprimé (-), fichier ajouter
git log --stat

# Afficher les statistiques en version compact
git log --shortstat

# Afficher les différences entre chaque commit (patch) pour les 5 derniers commits
git log -5 -p

echo azerty >> TODO.txt 
git add .
echo qwerty >> TODO.txt 

# Différence entre le répertoire de travail et l’index
git diff

# Différence entre l’index et le dernier commit (HEAD)
git diff --cached
 
# Différence entre le répertoire de travail et le dernier commit 
git diff HEAD

# Configuration d'un outil externe pour visualiser la différence
# Pour connaître les applications disponibles sur le système utilisable avec git difftoo
git difftool --tool-help

# On va utiliser Winmerge (https://winmerge.org/downloads/?lang=fr)
git config --global --add diff.tool winmerge
git config --global --add difftool.winmerge.path "C:/Program Files (x86)/WinMerge/WinMergeU.exe"
git config --global --add difftool.trustExitCode true
git config --global --add difftool.prompt false

# Une fois configuré difftool s'utilise comme la commande git did
git difftool 
git difftool --cached
git difftool HEAD

git grep -n "line-height"
git grep --count "line-height"
git grep -p "line-height"
git grep --break "line-height"

# git blame permet d’afficher l'auteur de la dernière modification de chaque ligne d'un fichier
git blame README.md
 
# -L pour limiter la sortie à la plage des lignes
# afficher le rsultat de git blame de la ligne 1 à 50
git blame README.md  -L 1,50

nano README.md # on indente le fichier README.md
git add. 
git commit -m "indentation ligne 29 à 35"

# Comme j'ai modifié des lignes en ajoutant des tabulations, des espaces, des retours à la ligne
git blame README.md  -L 1,50 # -> je deviens l'auteur de ces lignes
# l'option -w va ignorer tous les changements apportés aux espaces, aux tabulations , des nouvelle ligne … )
# C'est l'auteur d'origine de la ligne qui va être affiché
git blame README.md  -L 20,45 -w

nano README.md # on déplace les  lignes 3 à 7  premières lignes dans README.md de 21 lignes 
git add. 
git commit -m "copie ligne 3 à 7 de ligne 24 à 28"
git blame README.md  -L 20,45 # -> je deviens l'auteur de ces lignes
# l'option -M détecte les lignes déplacées ou copiées dans un même fichier et affiche l'auteur initial des lignes
git blame README.md -L 20,45 -M

# On copie les lignes 24 à 28 de README.md à la fin du fichier TODO.TXT
git add .
git commit -m "copie README.md -> TODO.txt"
git blame TODO.txt # -> je deviens l'auteur de ces lignes
# l'option -C détecte les lignes déplacées ou copiées dans un autre fichier et affiche l'auteur initial des lignes
git blame -C TODO.txt

```

## git bissect
On décompresse le fichier gitbissect.zip dans le dossier 05-gitbissect
```sh
cd ..
cd 05-gitbissect
# git bisect permet de rechercher à partir de quel commit un bug est apparu

# dans le fichier Test.java à ligne 12, il y a une erreur il manque this. devant val=val;  
# pour démarrer la recherche sur le commit qui pose problème (le dernier)
git bisect start
git bisect bad

git log
# ensuite un commit où le problème n’apparaît pas
git bisect good 19aaaf69ac3c

# ensuite pour chaque commit proposé par git, on va indiquer
# si l’erreur n’est pas présente -> git bisect good
# si elle est présente -> git bisect bad
git bisect good
git bisect bad
git bisect bad
# une fois le commit où apparait l'erreur est trouvé,  pour quitter git bissect -> git bisect reset
git bisect reset
```
## Désignation des commits

```sh
cd ..
cd 01-base/
git log
# pour les commandes qui attendent en paramètre un commit on peut désigner le commit avec:

# - sa signature SHA1 (complète ou abrégé) 
git show 5f4aaa0782e5ed27a7
git log

# - le nom d'une branche (ou d'un tag), dans ce cas le commit sera le commit où se trouve la branche
git show main

# - HEAD -> Fait référence à la branche courante, C’est le parent du prochain commit
git show HEAD
git log

# - ~ -> Ancêtre du dernier commit
git show ed6376fad3a16c87~ # parent du commit qui a la signature ed6376fad3a16c87
git show HEAD~ # HEAD~ -> avant dernier commit
git show HEAD~~ # -> avant avant dernier commit
git show HEAD~2 # -> idem
```