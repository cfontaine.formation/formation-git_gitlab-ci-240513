# Cycle de vie du répertoire de travail
```sh
# Dans le projet 01-base
# Création de 2 fichiers dans le répertoire de travail 
touch User.java
touch Article.java

# Vérifier l’état des fichiers 
git status # -> 2 fichiers Untracked (non-suivie)

# Ajout du fichier Article.java à l'index
git add Article.java
git status # Article.java -> indexer, User.java -> non-suivie

# Ajout du fichier User.java à l'index
git add User.java 
git status # Article.java et User.java -> indexer

# Création d'un commit avec les 2 fichiers ajouter dans l'index 
git commit # ouverture de l'éditeur de texte pour saisir le message du commit
git status # nothing to commit, working tree clean les fichiers du répertoire correspondent à ceux du commit

# Ajout dans le fichier Article.java
# public class Article{
# 	private String description;
#	private double prix; 
# }

# Ajout de la modification dans l'index
git add Article.java 
git status	# -> modified:   Article.java
git commit
git status

git log # Afficher la liste des commits 
git status -s # git status version compacte

# Ajout dans le fichier User.java
# public class User{
# 	private String prenom;
#   private String nom;
# }

git status -s	#  M User.java M -> modifier
				
# Ajout de plusieurs fichiers à l'index: User.java
git status -s 	# M  User.java  A -> ajouter

# L'option -m permet de saisir directement le message de validation
git commit -m "Ajout variables d'instance User" 
git status -s

# Modifier User.java
# public class User{
# 	private String prenom;
#   private String nom;
#   
#   public User(){
#   }	
# }
#
# Modifier Article.java
# public class Article{

#  // Variable d'instance
# 	private String description;
#	private double prix; 
#
#   // Constructeur par défaut
#	public Article(){
#	}
# }

# Ajouter tous les fichiers du répertoire dans l’index
git add .
git status -s # M  Article.java
			  # M  User.java 
git commit -m "Ajout des constructeurs par défaut"

# Commande plomberie pour visualiser le contenu de l’index
git ls-files --stage

# Modifier User.java
# public class User{
# 	private String prenom;
#   private String nom;
#   
#	// Constructeur par défaut
#   public User(){
#   }	
# }
#
# Modifier Article.java
# public class Article{
# 	private String description;
#	private double prix; 
#
#	// construteur par défaut  
#	public Article(){
#	}
# }

touch LICENCE.md

git status -s
#  M Article.java
# ?? LICENCE.md
#  M User.java

# Commiter tous les fichiers suivis du répertoire de travail sans passer par l’index
git commit -a -m "Ajout Commentaire"
git status -s
# ?? LICENCE.md	# LICENCE.md n'est pas suivi, il ne fait pas parti du nouveau commit

mkdir src		# On crée un dossier vide
git status -s	# Git ne versionne pas les répertoires vide
cd src
touch .gitkeep	# Si on ajoute un fichier dans le répertoire src, il est pris en compte par git
cd ..
git status -s
# ?? src/

git add .
git status -s
git commit -m "Ajout de la licnece Ajout du dossier src"

git status -s
git add src/
git add Licence.md 
echo GPL 3 >> Licence.md 
git status -s
# MM Licence.md 
# Si on fait le commit maintenant "GPL 3" ne fera pas partie du commit: uniquement un fichier Licence.md vide
git add Licence.md # Il faut indexer la modification pour qu'elle fasse partie du commit
git status -s
git commit -m "ajout de la licence"

# Visualiser les informations d'un commit
git show # par défaut, le dernier commit réalisé (HEAD)

# Visualiser les informations d'un commit en particulier,
# en utilisant sa signature sha-1 pour le désigner
git show 91602620197f269062834f75080783242159c85a

# --name-only -> affiche la liste des fichiers modifiés
git show --name-only 91602620197f269062834f75080783242159c85a

# --name-status -> affiche la liste des fichiers affectés
git show --name-status 91602620197f269062834f75080783242159c85a

# --abbrev-commit -> affiche que les 1er caractères de la somme de contrôle SHA-1
git show --abbrev -commit 91602620197f269062834f75080783242159c85a

# pour désigner les commits, on peut utiliser une signature sha-1 abrégée ( 8 premier caractères, 12 pour les gros projet)
git show --abbrev-commit 91602620197f269062834f75080783242159c85a

# --relative-date -> affiche la date en format relatif (10 minutes, 3 mois, ...)
git show --relative-date 91602620

# Modifier User.java
# public class User{
# 	private String prenom;
#   private String nom;
#   private LocalDate dateNaissance;
#   
#	// construteur par défaut
#   public User(){
#   }	
#
#   public User(String prenom, String nom){
#		this.prenom=prenom;
#		this.nom=nom;
#   }	
# }

git status -s
git add User.java # On ajoute User.java à l'index
git status -s

# Indexation interactive 
git add -i

# Affiche un menu:
#            staged     unstaged path
#  1:        +6/-0      nothing User.java
#
# *** Commands ***
#  1: status       2: update       3: revert       4: add untracked
#  5: patch        6: diff         7: quit         8: help

# 1-> afficher l'état du fichier (git status)
# 2-> ajouter à l'index un ou plusieurs déjà suivie
# 3-> retirer de l'index d'un ou plusieurs fichiers 
# 4-> Ajouter à l'index d'un ou plusieurs fichiers non suivie
# 5-> Indexation partielle d'un ou plusieurs fichiers
# 6-> Afficher la différence 

# Exemple:
 
# - Pour annuler l'indexation de User.java
#	3

#            staged     unstaged path
#  1:        +6/-0      nothing User.java
# Revert>> 
# on sélectionne les fichiers à annuler ici User.java -> 1

#            staged     unstaged path
# * 1:        +6/-0      nothing User.java
# Revert>>  # une fois tous les fichiers sélectionnés, on confirme en appuyant sur entrée

# - Pour faire une indexation partielle
# faire un commit qui ne contient que: private LocalDate dateNaissance;
# What now> 5
#            staged     unstaged path
#   1:    unchanged        +6/-0 User.java
# Patch update>> 1
#            staged     unstaged path
# * 1:    unchanged        +6/-0 User.java
# Patch update>> # On confirme avec entrée

# Git va afficher se qu'il y a indexer 
# public class User{
#    private String prenom;
#    private String nom;
# +  private LocalDate dateNaissance;

#   public User(){
#   }
#   
# +   public User(String prenom, String nom){
# +               this.prenom=prenom;
# +               this.nom=nom;
# +   }
# +
# }
#\ No newline at end of file
#(1/1) Stage this hunk [y,n,q,a,d,s,e,?]? # -> 

# y -> pour indexer ce qui est proposé par git
# n -> ne pas indexer do not stage this hunk
# s -> sous-diviser
# e -> editer manuellemnt
# ? -> afficher help

# git n'a pas réussit à diviser automatiquement le fichier
# (1/1) Stage this hunk [y,n,q,a,d,s,e,?]? s # choix -> s

# +  private LocalDate dateNaissance;
# (1/2) Stage this hunk [y,n,q,a,d,j,J,g,/,e,?]? y # on ajoute cette partie à l'index

# +   public User(String prenom, String nom){
# +               this.prenom=prenom;
# +               this.nom=nom;
# +   }
# +
# (2/2) Stage this hunk [y,n,q,a,d,K,g,/,e,?]? n # on ne 'ajoute pas à l'index

git commit -m "Ajout de la date de naissance"

# ------------------------------------------
# Si git ne propose pas l'option s (split)
git add -p
(1/1) Stage this hunk [y,n,q,a,d,e,?]?
# Ici git ne divise automatiquement le fichier et ne propose l'option split
# il faut editer manuellement -> e
# pour indexer les lignes on place + devant
# pour ignorer, on place # devant

# exemple pour ne pas indexer le 2ème constructeur
# #       public User(String prenom, String nom, LocalDate dateNaissance)
# #               this(prenom,nom);
# #               this.dateNaissance=dateNaisance;
# #       }
# #

# ------------------------------------------
git add User.java # Pour indexer la deuxième partie
git commit -m "autre constructeur"
git status -s

# Définir un fichier qui contient un modèle (template) pour le message, pour rappeler la structure du message
git config --global commit.template C:/Dawan/Formations/git/commit_template.txt

# commit_template.txt

# titre
# 
# corps du message

echo "Projet commande de base" >> README.md 
git add README.md 
git commit
# Dans l'éditeur, le message est initialisé avec le modèle du message
# /!\ le contenu du modèle de message fera parti du message, s'il n'est pas effacé ou en commentaire

# Pour supprimer la valeur d’un paramètre de configuration -> --unset
# exemple supprimer le template de message
git config --global --unset commit.template
git config --list

# Supprimer un fichier suivie -> git rm
git rm README.md
ls # avec git rm le fichier est supprimé du répertoire de travail
git status -s # D  README.md
git commit

touch server.log		
git add .
git commit

# avec l'option --cached le ficher le fichier ne sera pas supprimé du répertoire de travail
git rm --cached server.log 
git status -s # D  README.md
git commit 

# on ne peut pas supprimer un fichier indexé, il faut forcer la suppression avec -f (ou utilisé --cached)
touch config.xml
git add config.xml 
git rm config.xml # error: the following file has changes staged in the index: config.xml
git rm -f config.xml 

# Déplacer un fichier
git mv Article.java src/Article.java
git status -s
git mv *.java src/
git status -s
git rm src/.gitkeep # on peut supprimer .gitkeep, il n'est plus utile car le dossier n'est plus vide
git commit -m "reorganisation du projet"
```