# Formation Git : Gestion de dépôt
1. [Configuration de git et création d'un dépôt](https://gitlab.com/cfontaine.formation/formation-git_gitlab-ci-240513/-/blob/main/Configuration%20de%20git%20et%20cr%C3%A9ation%20d'un%20d%C3%A9p%C3%B4t.md?ref_type=heads)
2. [Cycle de vie du répertoire de travail](https://gitlab.com/cfontaine.formation/formation-git_gitlab-ci-240513/-/blob/main/Cycle%20de%20vie%20du%20r%C3%A9pertoire%20de%20travail.md?ref_type=heads)
3. [Ignorer des fichiers](https://gitlab.com/cfontaine.formation/formation-git_gitlab-ci-240513/-/blob/main/Ignorer%20des%20fichiers.md?ref_type=heads)
4. [Visualiser l'historique](https://gitlab.com/cfontaine.formation/formation-git_gitlab-ci-240513/-/blob/main/Visualiser%20l'historique.md?ref_type=heads)
5. [Annuler des actions](https://gitlab.com/cfontaine.formation/formation-git_gitlab-ci-240513/-/blob/main/Annuler%20des%20actions.md?ref_type=heads)
6. [Travailler avec les branches](https://gitlab.com/cfontaine.formation/formation-git_gitlab-ci-240513/-/blob/main/Travailler%20avec%20les%20branches.md?ref_type=heads)
7. [Travailler avec un dépôt distant](https://gitlab.com/cfontaine.formation/formation-git_gitlab-ci-240513/-/blob/main/Travailler%20avec%20un%20d%C3%A9p%C3%B4t%20distant.md?ref_type=heads)